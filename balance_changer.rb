conf = """upstream loadbalancer {
		server 172.17.0.1:5001 weight=#{ARGV[0]};
		server 172.17.0.1:5002 weight=#{ARGV[1]};
	}
	server {
		location / {
		proxy_pass http://loadbalancer;
		}
	}
"""

File.open('./nginx/nginx.conf', 'w') { |file| file.write(conf) }